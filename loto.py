import random
from functools import reduce

BARRELS_COUNT = 90
CARD_ROWS_COUNT = 3
CARD_COLUMNS_COUNT = 5


def main():
    my_card = generate_card()
    computer_card = generate_card()
    play_loto(my_card, computer_card)


def generate_card():
    card_list_len = CARD_ROWS_COUNT * CARD_COLUMNS_COUNT
    initial_list = random.sample(range(1, BARRELS_COUNT + 1), card_list_len)
    card = [list(map(str, sorted(initial_list[i:i + CARD_COLUMNS_COUNT])))
            for i in range(0, card_list_len, CARD_COLUMNS_COUNT)]
    flatten = reduce(lambda x, y: x + y, card)
    return flatten


def play_loto(card1, card2):
    # game loop
    barrels = list(map(str, random.sample(range(1, BARRELS_COUNT + 1), BARRELS_COUNT)))
    total_crossed1 = 0
    total_crossed2 = 0
    card_len = CARD_ROWS_COUNT * CARD_COLUMNS_COUNT

    while True:
        next_barrel = random.choice(barrels)
        barrels.remove(next_barrel)
        print_card("Ваша карточка", card1)
        print_card("Карточка компьютера", card2)
        print("Новый боченок: {}".format(next_barrel))

        # ask loop
        while True:
            answer = input("Зачеркнуть цифру? (Y/n)")
            if answer == 'y':
                card1_cross = True
            elif answer == 'n':
                card1_cross = False
            else:
                continue
            new_card1 = card_play(card1, next_barrel, card1_cross)
            card2_cross = should_cross(card2, next_barrel)
            new_card2 = card_play(card2, next_barrel, card2_cross)

            if not new_card1:
                print("Player 2 won!")
                return
            if not new_card2:
                print("Player 1 won!")
                return

            card1 = new_card1
            card2 = new_card2

            if card1_cross:
                total_crossed1 += 1
            if card2_cross:
                total_crossed2 += 1

            if total_crossed1 == total_crossed2 and total_crossed1 == card_len:
                print("Draw!")
                return
            elif total_crossed1 == card_len:
                print("Player 1 won!")
                return
            elif total_crossed2 == card_len:
                print("Player 2 won!")
                return

            break


def print_card(title, card):
    card_matrix = [card[i:i + CARD_COLUMNS_COUNT]
                   for i in range(0, len(card), CARD_COLUMNS_COUNT)]
    final_title = "---- {} ----".format(title)
    print(final_title)
    print('\n'.join([' '.join(c) for c in card_matrix]))
    print("-" * len(final_title))


def card_play(card, number, do_cross):
    if do_cross:
        if number in card:
            return ['-' if x == number else x for x in card]
        else:
            return None
    else:
        if number in card:
            return None

    return card


def should_cross(card, number):
    return number in card


if __name__ == '__main__':
    main()
