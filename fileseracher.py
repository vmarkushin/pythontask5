import sys
import os
import hashlib
import glob


def get_file_checksum(filename):
    hash_md5 = hashlib.md5()
    with open(filename, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def find_similar_files(filename, search_path):
    checksum = get_file_checksum(filename)
    files_checksums = {
        checksum: [filename]
    }

    for filepath in glob.iglob(search_path + '\\**', recursive=True):
        if not os.path.isfile(filepath):
            continue
        checksum = get_file_checksum(filepath)
        if checksum in files_checksums:
            files_checksums[checksum].append(filepath)
            print("Similar files with md5 {}: {}".format(checksum, files_checksums[checksum]))
        else:
            files_checksums[checksum] = [filepath]

    print(checksum)


def main():
    if len(sys.argv) != 3:
        print('usage: ./filesearcher.py file-to-read path-to-search')
        sys.exit(1)
    filename = sys.argv[1]
    search_path = sys.argv[2]
    find_similar_files(filename, search_path)


if __name__ == '__main__':
    main()
